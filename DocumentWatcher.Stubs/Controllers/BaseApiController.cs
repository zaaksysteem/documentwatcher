﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security;
using System.Web.Http;

namespace DocumentWatcher.Stubs.Controllers
{
    public abstract class BaseApiController : ApiController
    {
        protected void ValidateAuthentication()
        {
            var isAuthenticated = false;

            if (Request.Headers.Contains(Shared.Settings.Default.AuthenticationHeader))
            {
                var token = Request.Headers.GetValues(Shared.Settings.Default.AuthenticationHeader).First();
                if (!string.IsNullOrWhiteSpace(token) && token.Length == 16 && token.All(x => (x >= '0' && x <= '9') || (x >= 'a' && x <= 'f') || (x >= 'A' && x <= 'F')))
                {
                    isAuthenticated = true;
                }
            }

            if (!isAuthenticated)
            {
                throw new SecurityException("Request is not authenticated");
            }
        }

        internal static string ResourceDirectory
        {
            get
            {
                var assemblyPath = Uri.UnescapeDataString(new UriBuilder(Assembly.GetExecutingAssembly().CodeBase).Path);
                return $"{Path.GetDirectoryName(assemblyPath)}{Path.DirectorySeparatorChar}..{Path.DirectorySeparatorChar}Files";
            }
        }
    }
}