﻿using DocumentWatcher.IO.Data;
using DocumentWatcher.IO.Interfaces;
using DocumentWatcher.Services;
using DocumentWatcher.Services.Interfaces;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Threading;
using Microsoft.Practices.ServiceLocation;

namespace DocumentWatcher
{
    internal static class AppStartup
    {
        public static void Register()
        {
            // register services
            SimpleIoc.Default.Register<IChannelService, ChannelService>();
            SimpleIoc.Default.Register<IDatabase, DataFile>();
            SimpleIoc.Default.Register<IDocumentService, DocumentService>();
            SimpleIoc.Default.Register<IFileWatcherService, FileWatcherService>();
            SimpleIoc.Default.Register<IWorkService, WorkService>();
        }

        public static void Configure()
        {
            // map IOC provider
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            // enable invoking on UI thread
            DispatcherHelper.Initialize();
        }
    }
}