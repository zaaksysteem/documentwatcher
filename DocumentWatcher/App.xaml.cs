﻿using DocumentWatcher.Models;
using DocumentWatcher.Services;
using DocumentWatcher.Services.Interfaces;
using DocumentWatcher.Shared.Enums;
using DocumentWatcher.Views;
using GalaSoft.MvvmLight.Threading;
using Hardcodet.Wpf.TaskbarNotification;
using log4net;
using Microsoft.Practices.ServiceLocation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Windows.Media.Imaging;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using DocumentWatcher.IO;

namespace DocumentWatcher
{
    /// <summary>
    /// Interaction logic
    /// </summary>
    public partial class App: IDisposable
    {
        #region fields

        private static ILog Log => LogManager.GetLogger(typeof(App));
        private IChannelService _channelService;
        private bool _isExit;
        private Mutex _mutex;
        private IWorkService _workService;

        #endregion fields

        protected override void OnStartup(StartupEventArgs e)
        {
            // Initialize logging
            log4net.Config.XmlConfigurator.Configure();

            Initialize(out var isNewInstance);

            ProcessCommand(e.Args, isNewInstance);
            Log.Debug($"Started DocumentWatcher version '{Assembly.GetExecutingAssembly().GetName().Version}'");
            if (Common.CustomTempDirectorySet)
            {
                Log.Warn($"DocumentWatcher is using another temp directory than the default, current temp directory: '{Common.HomeDirectory}'");
            }
        }

        #region private helpers

        private void Initialize(out bool isNewInstance)
        {
            isNewInstance = IsNewInstance();
            if (isNewInstance)
            {
                Exit += App_Exit;

                AppStartup.Configure();
                AppStartup.Register();

                #region notifyIcon

                var notifyIcon = new TaskbarIcon
                {
                    ContextMenu = new ContextMenu { HasDropShadow = true, StaysOpen = false },
                    Icon = DocumentWatcher.Properties.Resources.zaak,
                    ToolTipText = Shared.Resources.NotifyTitle
                };
                var logoIco = new BitmapImage();
                logoIco.BeginInit();
                logoIco.UriSource = new Uri("pack://application:,,,/DocumentWatcher;component/Images/Icons/zaak.ico");
                logoIco.EndInit();
                ((MenuItem)notifyIcon.ContextMenu.Items.GetItemAt(notifyIcon.ContextMenu.Items.Add(new MenuItem { Header = Shared.Resources.MenuStatus, FontWeight = FontWeights.Bold }))).Click += (s, e) => ShowMainWindow();
                ((MenuItem)notifyIcon.ContextMenu.Items.GetItemAt(notifyIcon.ContextMenu.Items.Add(new MenuItem { Header = Shared.Resources.MenuVersion }))).Click += (s, e) => AlertMessage(notifyIcon, $"{Shared.Resources.MenuVersion} {Assembly.GetExecutingAssembly().GetName().Version}", MessageType.None);
                ((MenuItem)notifyIcon.ContextMenu.Items.GetItemAt(notifyIcon.ContextMenu.Items.Add(new MenuItem { Header = Shared.Resources.MenuZaaksysteemWeb, Icon = new Image { Source = logoIco, Width = 16, Height = 16} }))).Click += (s, e) => Process.Start("https://www.zaaksysteem.nl");
                ((MenuItem)notifyIcon.ContextMenu.Items.GetItemAt(notifyIcon.ContextMenu.Items.Add(new MenuItem { Header = Shared.Resources.MenuClose }))).Click += (s, e) => ExitApplication();
                notifyIcon.TrayMouseDoubleClick += (s, e) => ShowMainWindow();
                notifyIcon.TrayBalloonTipClicked += (s, e) => ShowMainWindow();

                #endregion notifyIcon

                MainWindow = new MainWindow();
                MainWindow.Closing += MainWindow_Closing; // a hidden window can be shown again, a closed one not
                ((MainWindow)MainWindow).WorkAdded += e => AlertMessage(notifyIcon, string.Format(Shared.Resources.NotifyInfoWorkAdded, e, CultureInfo.CurrentCulture), MessageType.Info);

                // start listening to other applications
                _channelService = ServiceLocator.Current.GetInstance<IChannelService>();
                _channelService.StartChannel();

                // register alert system
                _workService = ServiceLocator.Current.GetInstance<IWorkService>();
                _workService.Alert += (message, type) => AlertMessage(notifyIcon, message, type);
                _workService.ConfirmWork += RetainConfirm;
                _workService.CleanWork();

                // splash
                AlertMessage(notifyIcon, Shared.Resources.NotifyStartApplication, MessageType.None);
            }
        }

        private void ProcessCommand(IReadOnlyList<string> args, bool isNewInstance)
        {
            var parsed = ParseArguments(args, out var instruction);
            if (parsed.GetValueOrDefault())
            {
                // forward work to main application
                if (isNewInstance)
                {
                    Log.Debug("Opening new process instance to communicate from");
                    Process.Start(Assembly.GetExecutingAssembly().Location, args[0]);
                }
                else
                {
                    Log.Info($"Receiving command {(instruction.Version == 2 ? instruction.BaseAddress : instruction.Download)} with onetime token {instruction.AuthenticationToken}");
                    ChannelService.PublishWork(instruction);
                }
            }
            else if (parsed.HasValue && args.Count > 0 || !isNewInstance)
            {
                ShowUsage();
            }

            if (!isNewInstance)
            {
                Shutdown();
            }
        }

        private static bool? ParseArguments(IReadOnlyList<string> args, out Instruction instruction)
        {
            instruction = null;

            if (args.Count != 1 || !args[0].StartsWith(Shared.Settings.Default.ProtocolHandler, StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }

            Log.Debug($"Receiving command line argument <ARG>{args[0]}</ARG>");
            var argStripped = args[0].Substring(Shared.Settings.Default.ProtocolHandler.Length, args[0].Length - Shared.Settings.Default.ProtocolHandler.Length).TrimEnd('/');

            try
            {
                var json = Encoding.UTF8.GetString(Convert.FromBase64String(argStripped));
                instruction = JsonConvert.DeserializeObject<Instruction>(json);
                instruction.Init();
            }
            catch
            {
                instruction = new Instruction();
            }

            return instruction?.IsValid();
        }

        private static void AlertMessage(TaskbarIcon notifyIcon, string message, MessageType type)
        {
            Log.Debug($"AlertMessage triggered for type '{type}' with message '{message}'");
            switch (type)
            {
                case MessageType.Info:
                    notifyIcon.ShowBalloonTip(Shared.Resources.NotifyTitle, message, BalloonIcon.Info);
                    break;
                case MessageType.Warning:
                    notifyIcon.ShowBalloonTip(Shared.Resources.NotifyTitle, message, BalloonIcon.Warning);
                    break;
                case MessageType.Error:
                    notifyIcon.ShowBalloonTip(Shared.Resources.NotifyTitle, message, BalloonIcon.Error);
                    break;
                default:
                    notifyIcon.ShowBalloonTip(Shared.Resources.NotifyTitle, message, BalloonIcon.None);
                    break;
            }
        }

        private void RetainConfirm(Work oldWork)
        {
            DispatcherHelper.CheckBeginInvokeOnUI(delegate
            {
                if (new ConfirmWindow(oldWork).ShowDialog().GetValueOrDefault())
                {
                    _workService.RetainWork(oldWork);
                }
                else
                {
                    oldWork.State = State.Done;
                }
            });
        }

        private static void ShowUsage()
        {
            Log.Error(@"Usage " + Shared.Settings.Default.ProtocolHandler + @"<Base64Encoded UTF-8 JSON-string>
            e.g.:
            {
              ""base_uri"":""gemeente.zaaksysteem.nl/api/v1/file/d87058ec-d5b2-11e7-a786-c2a3b93565c4"",
              ""auth_token"":""fefdaaf48dfa9ca2"",
              ""version"":2,
              ""download"":""download"",
              ""upload"":""upload"",
              ""lock_get"":""lock"",
              ""lock_acquire"":""lock/acquire"",
              ""lock_extend"":""lock/extend"",
              ""lock_release"":""lock/release""
            }");
        }

        private bool IsNewInstance()
        {
            _mutex = new Mutex(true, @"Local\ZaaksysteemDocumentWatcher", out var isNewInstance);
            return isNewInstance;
        }

        private void ShowMainWindow()
        {
            if (MainWindow.IsVisible)
            {
                if (MainWindow.WindowState == WindowState.Minimized)
                {
                    MainWindow.WindowState = WindowState.Normal;
                }

                MainWindow.Activate();
            }
            else
            {
                MainWindow.Show();
            }
        }

        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            if (!_isExit)
            {
                e.Cancel = true;
                MainWindow.Hide();
            }
        }

        private void ExitApplication()
        {
            _isExit = true;

            MainWindow.Close();
        }

        private void App_Exit(object sender, ExitEventArgs e)
        {
            Dispose();
        }

        #endregion private helpers

        #region IDisposable

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool managed)
        {
            if (managed)
            {
                _channelService.Dispose();

                if (_mutex != null)
                {
                    try
                    {
                        _mutex.ReleaseMutex();
                    }
                    catch { }

                    _mutex.Dispose();
                }
            }
        }

        #endregion IDisposable
    }
}