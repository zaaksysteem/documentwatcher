﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace DocumentWatcher.Models
{
    public class Instruction
    {
        [JsonProperty("base_uri")]
        public string BaseAddress { get; set; }
        [JsonProperty("auth_token")]
        public string AuthenticationToken { get; set; }
        public int? Version { get; set; }
        public string Download { get; set; }
        [JsonProperty("lock_extend")]
        public string ExtendLock { get; set; }
        [JsonProperty("lock_get")]
        public string HasLock { get; set; }
        [JsonProperty("lock_acquire")]
        public string SetLock { get; set; }
        [JsonProperty("lock_release")]
        public string Unlock { get; set; }
        public string Upload { get; set; }

        public string SessionId { get; set; }

        public string Host
        {
            get
            {
                if (Uri.TryCreate($"{Uri.UriSchemeHttps}://{BaseAddress}", UriKind.Absolute, out var hostUri))
                {
                    return hostUri.Host;
                }

                return null;
            }
        }

        public string UriScheme
        {
            get
            {
#if DEBUG
                if (Host == "localhost" || Host == "127.0.0.1" || Host?.ToLowerInvariant() == "pc27")
                {
                    return Uri.UriSchemeHttp;
                }
#endif
                return Uri.UriSchemeHttps;
            }
        }

        public Uri BaseUri
        {
            get
            {
                if (Uri.TryCreate($"{UriScheme}://{Host}", UriKind.Absolute, out var hostUri))
                {
                    return hostUri;
                }

                return null;
            }
        }

        public Guid FileId
        {
            get
            {
                if (IsValid())
                {
                    if (Version == 2)
                    {
                        if (Uri.TryCreate($"{UriScheme}://{BaseAddress}", UriKind.Absolute, out var baseAddressUri))
                        {
                            try
                            {
                                return new Guid(baseAddressUri.Segments[baseAddressUri.Segments.Length - 1]);
                            }
                            catch
                            { }
                        }
                    }
                    else
                    {
                        if (Uri.TryCreate(BaseUri, HasLock, out var hasLockUri))
                        {
                            try
                            {
                                var uriSegment = hasLockUri.Segments[hasLockUri.Segments.Length - 2];
                                return new Guid(uriSegment.Substring(0, uriSegment.Length - 1));
                            }
                            catch
                            {
                            }
                        }
                    }
                }

                return Guid.Empty;
            }
        }

        public void Init()
        {
            if (Version == 2)
            {
                Download = $"{Regex.Replace(BaseAddress, Host, string.Empty, RegexOptions.IgnoreCase)}/{Download}";
                ExtendLock = $"{Regex.Replace(BaseAddress, Host, string.Empty, RegexOptions.IgnoreCase)}/{ExtendLock}";
                HasLock = $"{Regex.Replace(BaseAddress, Host, string.Empty, RegexOptions.IgnoreCase)}/{HasLock}";
                SetLock = $"{Regex.Replace(BaseAddress, Host, string.Empty, RegexOptions.IgnoreCase)}/{SetLock}";
                Unlock = $"{Regex.Replace(BaseAddress, Host, string.Empty, RegexOptions.IgnoreCase)}/{Unlock}";
                Upload = $"{Regex.Replace(BaseAddress, Host, string.Empty, RegexOptions.IgnoreCase)}/{Upload}";
            }
            /* first version
            {
                "base_uri" : "gemeente.zaaksysteem.nl",
                "auth_token" : "419240cd3d90c5eb",
                "download" : "/api/v1/file/550e8400-e29b-41d4-a716-446655440000/download",
                "upload" : "/api/v1/file/550e8400-e29b-41d4-a716-446655440000/upload",
                "lock_get" : "/api/v1/file/550e8400-e29b-41d4-a716-446655440000/lock",
                "lock_acquire" : "/api/v1/file/550e8400-e29b-41d4-a716-446655440000/lock/acquire",
                "lock_extend" : "/api/v1/file/550e8400-e29b-41d4-a716-446655440000/lock/extend",
                "lock_release" : "/api/v1/file/550e8400-e29b-41d4-a716-446655440000/lock/release"
            }

            * version 2
            {
                "base_uri":"gemeente.zaaksysteem.nl/api/v1/file/d87058ec-d5b2-11e7-a786-c2a3b93565c4",
                "auth_token":"fefdaaf48dfa9ca2",
                "version":2,
                "download":"download",
                "upload":"upload",
                "lock_get":"lock",
                "lock_acquire":"lock/acquire",
                "lock_extend":"lock/extend",
                "lock_release":"lock/release"
            }
            */
        }

        public bool IsValid()
        {
            return (BaseUri?.Authority.Equals(Host, StringComparison.OrdinalIgnoreCase)).GetValueOrDefault()
                && (!string.IsNullOrWhiteSpace(AuthenticationToken) && AuthenticationToken.Length == 16 && AuthenticationToken.All(x => (x >= '0' && x <= '9') || (x >= 'a' && x <= 'f') || (x >= 'A' && x <= 'F')))
                && (Uri.TryCreate(BaseUri, Upload, out var uploadUri) && uploadUri.PathAndQuery.Equals(Upload, StringComparison.OrdinalIgnoreCase))
                && (Uri.TryCreate(BaseUri, Download, out var downloadUri) && downloadUri.PathAndQuery.Equals(Download, StringComparison.OrdinalIgnoreCase))
                && (Uri.TryCreate(BaseUri, HasLock, out var hasLockUri) && hasLockUri.PathAndQuery.Equals(HasLock, StringComparison.OrdinalIgnoreCase))
                && (Uri.TryCreate(BaseUri, SetLock, out var setLockUri) && setLockUri.PathAndQuery.Equals(SetLock, StringComparison.OrdinalIgnoreCase))
                && (Uri.TryCreate(BaseUri, ExtendLock, out var extendLockUri) && extendLockUri.PathAndQuery.Equals(ExtendLock, StringComparison.OrdinalIgnoreCase))
                && (Uri.TryCreate(BaseUri, Unlock, out var unlockUri) && unlockUri.PathAndQuery.Equals(Unlock, StringComparison.OrdinalIgnoreCase));
        }
    }
}