﻿using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("DocumentWatcher.Shared")]
[assembly: AssemblyDescription("Shared components")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Mintlab")]
[assembly: AssemblyProduct("DocumentWatcher.Shared")]
[assembly: AssemblyCopyright("EUPL https://eupl.eu/")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: NeutralResourcesLanguage("nl-NL")]

[assembly: CLSCompliant(true)]
[assembly: ComVisible(false)]
[assembly: Guid("35885ad2-9985-449b-9588-ac8a2c601e62")]

[assembly: AssemblyVersion("1.12.*")]
[assembly: AssemblyFileVersion("1.12")]