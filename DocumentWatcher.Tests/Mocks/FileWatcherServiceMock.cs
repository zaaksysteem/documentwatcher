﻿using DocumentWatcher.Services.Interfaces;

namespace DocumentWatcher.Tests.Mocks
{
    internal class FileWatcherServiceMock : IFileWatcherService
    {
        public void StartWatcher()
        {
        }

        public void StopWatcher()
        {
        }
    }
}