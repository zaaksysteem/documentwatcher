﻿using DocumentWatcher.Models;
using DocumentWatcher.Services.Interfaces;
using System;
using System.IO;

namespace DocumentWatcher.Tests.Mocks
{
    internal class DocumentServiceMock : IDocumentService
    {
        public Document CreateDocument((string fileName, byte[] binary) file)
        {
            return new Document
            {
                FilePath = file.fileName,
                Binary = file.binary
            };
        }

        public void OpenDocument(Document document)
        {
        }

        public Document GetDocument(string fileName)
        {
            var document = new Document();

            if (fileName == "Aanvraagformulier.xlsx")
            {
                // this file is probably available and modified on every windows client
                document.FilePath = Path.Combine(Environment.GetEnvironmentVariable("WINDIR"), "WindowsUpdate.log");
                document.Binary = File.ReadAllBytes(document.FilePath);
            }

            return document;
        }

        public void ActivateDocument(string fileName)
        {
        }

        public bool DeleteDocument(string filePath)
        {
            return true;
        }
    }
}