﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("DocumentWatcher.Services")]
[assembly: AssemblyDescription("Business logic")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Mintlab")]
[assembly: AssemblyProduct("DocumentWatcher.Services")]
[assembly: AssemblyCopyright("EUPL https://eupl.eu/")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: CLSCompliant(true)]
[assembly: ComVisible(false)]
[assembly: Guid("4271f9e4-f21a-4984-8b8c-ff18e33f31f1")]

[assembly: AssemblyVersion("1.12.*")]
[assembly: AssemblyFileVersion("1.12")]